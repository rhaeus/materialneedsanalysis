/**
 * 
 */
package edu.kit.informatik.analysis.integrity;

import java.util.List;
import java.util.Objects;

import edu.kit.informatik.analysis.BomEntry;
import edu.kit.informatik.analysis.Unit;
import edu.kit.informatik.analysis.UnitType;
import edu.kit.informatik.analysis.util.Util;

/**
 * An Implementation of BomIntegrity to ensure the integrity of the material needs analysis system
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class IntegrityChecker implements BomIntegrity {

    @Override
    public boolean checkForCycle(Unit toAddTo, List<Unit> toAddList) {
        for (Unit toAdd : toAddList) {
            //if unit to add to is same as unit that should be added cycle is created
            if (toAdd.equals(toAddTo)) {
                return true;
            }
            
            //if component is added, no cycle can be produced
            if (toAdd.getType() == UnitType.COMPONENT) {
                continue;
            }
            
            //if unit that should be added is already present in unit to which it should be added its ok
            //can happen only in addPart, and addPart can't change a unit from component to assembly
            if (Util.bomEntryUnitExists(toAdd.getName(), toAddTo.getAssembliesRaw())) {
                continue;
            }
            
            //if toAdd contains toAddTo -> cycle
            List<BomEntry> allSubs = toAdd.getAssembliesRaw(); 
            allSubs.addAll(toAdd.getComponentsRaw()); //consider components too because toAddTo could be component  
                                                      //at the moment, but afterwards (addAssembly) an assembly
            if (Util.bomEntryUnitExists(toAddTo.getName(), allSubs)) {
                return true;
            }
        }       
        return false;
    }

    @Override
    public void removeUnusedComponents(List<Unit> componentsToCheck, List<Unit> inventory) {
        Objects.requireNonNull(componentsToCheck);
        Objects.requireNonNull(inventory);
        
        //check if only components provided
        for (Unit unit : componentsToCheck) {
            if (unit.getType() != UnitType.COMPONENT) {
                throw new IllegalArgumentException("Given units to check must be of type component.");
            }
        }
        
        //check for each provided component if it is used by another assembly
        for (Unit component : componentsToCheck) {
            boolean unused = true;
            //traverse inventory and check for each entry if it contains the component directly
            //it is sufficient to check only the first level of each unit because all units are checked ->
            //the products consists of units hierarchically -> all levels are checked
            for (Unit unit : inventory) {
                if (unit.usesUnitWithNameDirectly(component.getName())) {
                    unused = false;
                    break;
                }
            }
            
            if (unused) {
                //remove component from inventory if not used anymore
                inventory.remove(component);
            }
        }
    }
}
