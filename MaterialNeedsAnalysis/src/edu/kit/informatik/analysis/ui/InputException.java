/**
 * 
 */
package edu.kit.informatik.analysis.ui;

/**
 * Custom Exception that can be thrown if a user input is not valid.
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class InputException extends Exception {

    /*
     * generated serialVersionUID
     */
    private static final long serialVersionUID = -7276527289448774665L;

    /**
     * The constructor of the DawnGameInputException that is thrown if a user input is
     * invalid.
     *
     * @param message The error message to display to the user.
     */
    public InputException(String message) {
        super(message);
    }
}
