/**
 * 
 */
package edu.kit.informatik.analysis.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.analysis.BomEntry;
import edu.kit.informatik.analysis.BomManagement;
import edu.kit.informatik.analysis.Unit;

/**
 * Contains the available commands for the interaction with the material needs analysis system
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum Command {
    /**
     * Represents the add assembly command. 
     * Adds an assembly to the system
     */
    ADD_ASSEMBLY(RegexPatterns.REGEX_ADD_ASSEMBLY) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains parameter assembly name
            String assemblyName = matcher.group(1);
            //fetch parameter
            //group 0 contains whole input
            String[] params = matcher.group(0).split("=")[1].split(";"); //contains <amount>:<name>
            List<BomEntry> subAssemblies = new ArrayList<BomEntry>();
            //fetch all assemblies with amount to add them to the system
            for (String entry : params) {
                String[] split = entry.split(":");
                subAssemblies.add(new BomEntry(new Unit(split[1]), Integer.parseInt(split[0])));
            }
            bomMan.addAssembly(assemblyName, subAssemblies);
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the remove assembly command.
     * Removes an assembly from the system
     */
    REMOVE_ASSEMBLY(RegexPatterns.REGEX_REMOVE_ASSEMBLY) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains name of assembly to remove
            bomMan.removeAssembly(matcher.group(1));
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the print assembly command.
     * Prints the direct subassemblies/components of an assembly
     * ordered: ascending after subassembly/component name.
     */
    PRINT_ASSEMBLY(RegexPatterns.REGEX_PRINT_ASSEMBLY) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains name of assembly to print
            Terminal.printLine(bomMan.printAssembly(matcher.group(1))); 
        }
    },
    
    /**
     * Represents the get assemblies command.
     * Prints the direct and indirect subassemblies of an assembly with their amount of usage
     * ordered: descending after amount, then ascending after subassembly name.
     */
    GET_ASSEMBLIES(RegexPatterns.REGEX_GET_ASSEMBLIES) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains name of assembly to get sub assemblies from
            Terminal.printLine(bomMan.getAssemblies(matcher.group(1))); 
        }
    },
    
    /**
     * Represents the get components command.
     * Prints the direct and indirect components of an assembly with their amount of usage
     * ordered: descending after amount, then ascending after component name.
     */
    GET_COMPONENTS(RegexPatterns.REGEX_GET_COMPONENTS) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains name of assembly to get sub components from
            Terminal.printLine(bomMan.getComponents(matcher.group(1))); 
        }
    },
    
    /**
     * Represents the add part command.
     * Adds a component or subassembly to an assembly.
     */
    ADD_PART(RegexPatterns.REGEX_ADD_PART) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains the name of the assembly to which a part should be added
            String assemblyToAddTo = matcher.group(1);
            //group 2 contains the amount and name of the part to add
            String parameter = matcher.group(2);
            long amount = Long.parseLong(parameter.split(":")[0]); //amount to add
            String partToAdd = parameter.split(":")[1]; //name of part to add
            
            bomMan.addPart(assemblyToAddTo, new BomEntry(new Unit(partToAdd), amount));
            Terminal.printLine(MessageStrings.OK.toString());
        }
    },
    
    /**
     * Represents the remove part command.
     * Removes a component or subassembly from an assembly.
     */
    REMOVE_PART(RegexPatterns.REGEX_REMOVE_PART) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            //group 1 contains the name of the assembly from which a part should be removed
            String assemblyToRemoveFrom = matcher.group(1);
            //group 2 contains the amount and name of the part to remove
            String parameter = matcher.group(2);
            long amount = Long.parseLong(parameter.split(":")[0]); //amount to remove
            String partToRemove = parameter.split(":")[1]; //name of part to remove
            
            bomMan.removePart(assemblyToRemoveFrom, new BomEntry(new Unit(partToRemove), amount));
            Terminal.printLine(MessageStrings.OK.toString());  
        }
    },
    
    /**
     * Represents the quit command.
     * Allows the user to exit the program.
     */
    QUIT(RegexPatterns.REGEX_QUIT) {
        @Override
        public void execute(BomManagement bomMan) throws InputException {
            quit();
        }
    };
    
    /**
     * Matcher object for regex. Contains matched input to use for further evaluation, e.g. regex groups.
     */
    protected Matcher matcher;
    private final Pattern pattern;
    private boolean quit = false;
    
    /*
     * Constructor for Command enum. Stores pattern for regex matching.
     */
    private Command(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }
    
    
    /**
     * Checks if given command is quit command
     * 
     * @return true if command is quit command
     */
    public boolean isQuit() {
        return this.quit;
    }
    
    /**
     * Exits the program gracefully.
     */
    protected void quit() {
        quit = true;
    }
    
    /**
     * perform work of command
     * @param bomMan bom management object, to perform action on
     * 
     * @throws InputException if parameters are not valid according to the system definition
     */
    public abstract void execute(BomManagement bomMan) throws InputException;

    /**
     * Tries to match input to a command and starts executing
     * 
     * @param input input to match on command
     * @param bomMan bom management object on which to operate
     * @return matched command, if successful
     * @throws InputException if input couldn't be matched to a command
     */
    public static Command matchAndExecute(String input, BomManagement bomMan) throws InputException {
        for (Command command : Command.values()) {
            command.matcher = command.pattern.matcher(input);
            //if command matches input, perform work and return command object to caller
            if (command.matcher.matches()) {
                command.execute(bomMan);
                return command;
            }
        }

        //no command could be matched to input
        throw new InputException(MessageStrings.INVALID_CMD.toString());
    }
}
