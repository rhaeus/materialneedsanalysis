/**
 * 
 */
package edu.kit.informatik.analysis.ui;

/**
 * Contains strings with messages to display to the user
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum MessageStrings {
    /**
     * Message for user if ok.
     */
    OK("OK"),
    
    /**
     * Message for user if given command is not valid.
     */
    INVALID_CMD("Not a valid command."),
    
    /**
     * Message for user if an assembly already exists in the system.
     */
    ASSEMBLY_ALREADY_EXISTS("BOM with the specified name already exists."),
    
    /**
     * Message for user if a specified BOM would create a cycle in the product structure.
     */
    CYCLE_ERROR("The specified BOM would create a cycle in the product structure."),
    
    /**
     * Message for user if no BOM exists in the system for the specified name.
     */
    ASSEMBLY_NOT_EXIST("No BOM exists in the system for the specified name."),
    
    /**
     * Message for user if no BOM or component exists in the system for the specified name.
     */
    UNIT_NOT_EXIST("No BOM or component exists in the system for the specified name."),
    
    /**
     * Message for user if a specification of a BOM contains the same name multiple times.
     */
    NAME_DUPLICATES("The names of the parts in the specified BOM appear multiple times."),
    
    /**
     * Message for user if an assembly doesn't contain sub assemblies.
     */
    EMPTY("EMPTY"),
    
    /**
     * Message for user if he tries to add too many components or assemblies. 
     * The maximum amount for a sub component/assembly is limited to 1000.
     */
    TOO_MANY_SUBS("The amount is too high."),
    
    /**
     * Message for user if a BOM does not contain a specified part in a specified amount and therefore
     * removing this amount is not possible.
     */
    TOO_FEW_SUBS("The BOM does not contain the specified part in the specified amount."),
    
    /**
     * Message for user if a BOM does not contain a specified part.
     */
    PART_NOT_AVAILABLE("The BOM does not contain the specified part.");
    
    private final String message;
    
    /*
     * Private constructor to initialize enum object with it's message string
     */
    private MessageStrings(String message) {
        this.message = message;
    }
    
    @Override
    public String toString() {
        return message;
    }
}
