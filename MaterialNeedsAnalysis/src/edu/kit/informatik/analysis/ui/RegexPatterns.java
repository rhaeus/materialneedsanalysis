/**
 * 
 */
package edu.kit.informatik.analysis.ui;

/**
 * Holds Regex patterns for input validation for Material Needs Analysis
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public final class RegexPatterns {
    /**
     * Regex for assembly and component name
     * At least 1 character (upper or lower case)
     */
    public static final String REGEX_NAME_PATTERN = "([a-zA-z]+)";
    
    /**
     * Regex for amount of assembly/component to add.
     * Range [1, 1000]
     */
    public static final String REGEX_AMOUNT_PATTERN = "([1-9]([0-9]){0,2}|1000)";
    
    /**
     * Combined regex with amount and name.
     */
    public static final String REGEX_AMOUNT_NAME = "(" + REGEX_AMOUNT_PATTERN 
                                                        + ":"
                                                        + REGEX_NAME_PATTERN + ")";
    
    /**
     * Regex for add assembly command.
     * Must at least contain 1 amount and name entry.
     */
    public static final String REGEX_ADD_ASSEMBLY = "addAssembly " 
                                                        + REGEX_NAME_PATTERN 
                                                        + "=" 
                                                        + REGEX_AMOUNT_NAME
                                                        + "(;" + REGEX_AMOUNT_NAME + ")*";
    
    /**
     * Regex for remove assembly command.
     * Expects an assembly name as parameter.
     */
    public static final String REGEX_REMOVE_ASSEMBLY = "removeAssembly " + REGEX_NAME_PATTERN;
    
    /**
     * Regex for print assembly command.
     * Expects an assembly name as parameter.
     */
    public static final String REGEX_PRINT_ASSEMBLY = "printAssembly " + REGEX_NAME_PATTERN;
    
    /**
     * Regex for get assemblies command.
     * Expects an assembly name as parameter.
     */
    public static final String REGEX_GET_ASSEMBLIES = "getAssemblies " + REGEX_NAME_PATTERN;
    
    /**
     * Regex for get components command.
     * Expects an assembly name as parameter.
     */
    public static final String REGEX_GET_COMPONENTS = "getComponents " + REGEX_NAME_PATTERN;
    
    /**
     * Regex for add part command.
     * Expects 1 ammount and name entry.
     */
    public static final String REGEX_ADD_PART = "addPart "
                                                    + REGEX_NAME_PATTERN
                                                    + "\\+"
                                                    + REGEX_AMOUNT_NAME;
                                                    
    /**
     * Regex for remove part command.
     * Expects 1 ammount and name entry.
     */
    public static final String REGEX_REMOVE_PART = "removePart "
                                                    + REGEX_NAME_PATTERN
                                                    + "-"
                                                    + REGEX_AMOUNT_NAME;
    
    /**
     * Regex for quit command.
     * Command expects no parameters.
     */
    public static final String REGEX_QUIT = "quit";    
}
