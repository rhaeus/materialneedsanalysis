/**
 * 
 */
package edu.kit.informatik.analysis.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import java.util.Set;

import edu.kit.informatik.analysis.BomEntry;
import edu.kit.informatik.analysis.Unit;
import edu.kit.informatik.analysis.ui.MessageStrings;

/**
 * Contains some helper methods
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public final class Util {
    /**
     * Merges the given lists into one list by concatenating all elements
     * 
     * @param list lists to merge
     * @param <T> type of the list elements
     * @return merged list
     */
    @SafeVarargs
    public static <T> List<T> mergeLists(List<T>... list) {
        List<T> result = new ArrayList<>();
        for (List<T> l : list) {
            result.addAll(l);
        }
        return result;
    }
    
    /**
     * Checks if a List contains duplicate elements according to equals
     * 
     * @param list List to check for duplicates
     * @param <T> type of list object
     * @return true if list contains duplicates, false if not
     */
    public static <T> boolean listContainsDuplicates(List<T> list) {
        return list.size() != list.stream().distinct().count();
    }
    
    /**
     * Checks if a List of BomEntry contains duplicate elements according to the unit name
     * 
     * @param list List to check for duplicates
     * @return true if list contains duplicates, false if not
     */
    public static boolean duplicateUnitsInBomEntryList(List<BomEntry> list) {
        Set<String> set = new HashSet<>();
        for (BomEntry entry : list) {
            if (!set.add(entry.getUnit().getName())) {
                //set.add returns false if element already exists -> duplicate unit name
                return true;
            }
        }
        return false;
    }
    
    /**
     * Converts the given List of BomEntry into a list of Unit 
     * by extracting the units from the bomList and putting them into a new list
     * 
     * @param bomList list to convert
     * @return list that contains all units contained in the bomList (duplicates are not removed)
     */
    public static List<Unit> bomListToUnitList(List<BomEntry> bomList) {
        List<Unit> result = new ArrayList<>();
        for (BomEntry entry : bomList) {
            result.add(entry.getUnit());
        }
        return result;
    }

    /**
     * Creates a list that contains each unit wrapped in a BomEntry only once. If {@code rawList} contains 
     * multiple entries with the same unit, they are merged to one entry and their amounts are added up.
     * {@code rawList} is not modified, a new list gets returned.
     * This can be used to retrieve the bill of quantity for an assembly.
     * 
     * @param rawList all entries that should be merged
     * @return A list that contains each unit wrapped in a BomEntry only once with added up amounts
     */
    public static List<BomEntry> createMergedEntryList(List<BomEntry> rawList) {
        Map<String, BomEntry> merged = new HashMap<>(); //use hashmap for intermediate result because lookup is fast
        //for each entry in rawList traverse rawList and add up amount
        //store result in merged map and in further calculations skip entries with this unit
        //because it was already considered
        for  (BomEntry entry : rawList) {
            if (!merged.containsKey(entry.getUnit().getName())) {
                long addedAmount = 0;
                for (BomEntry e : rawList) {
                    if (e.getUnit().equals(entry.getUnit())) {
                        addedAmount += e.getUnitAmount();
                    }
                }
                //A IllegalArgumentException can occur if Long.MAX_VALUE is exceeded because
                //BomEntry only allows positive amount
                //But it was not part of the specification to consider overflow of Long.MAX_VALUE
                //In normal operation addedAmount will always be positive here
                merged.put(entry.getUnit().getName(), new BomEntry(entry.getUnit(), addedAmount));
            }
        }
        
        //Convert map to list for sorting and returning
        List<BomEntry> mergedList = new ArrayList<>(merged.values());
        Collections.sort(mergedList);
        return mergedList;
    }
    
    /**
     * Multiplies the amount of each entry in {@code list} by {@code amountFactor}.
     * The original list entries are not modified, a new list with new objects is created.
     * 
     * @param list list with entries to be multiplied
     * @param amountFactor factor to multiply the amount
     * @return a list that contains entries with multiplied amount
     */
    public static List<BomEntry> multiplyBomEntries(List<BomEntry> list, long amountFactor) {
        List<BomEntry> result = new ArrayList<>();
        for (BomEntry entry : list) {
            result.add(new BomEntry(entry.getUnit(), entry.getUnitAmount() * amountFactor));
        }
        return result;
    }
    
    /**
     * Checks if a unit with name {@code unitName} exists in {@code list} of BomEntry
     * 
     * @param unitName name of unit to check existence for
     * @param list BomEntry list to search for a unit with name {@code unitName}
     * @return true if present, false if not
     */
    public static boolean bomEntryUnitExists(String unitName, List<BomEntry> list) {
        return getBomEntryWithUnitName(unitName, list) != null;
    }
    
    /**
     * Searches for an entry with name {@code unitName} in list {@code list} and returns the object if found
     * 
     * @param unitName name of unit to search
     * @param list BomEntry list to search for a unit with name {@code unitName}
     * @return BomEntry object which contains a unit with name {@code unitName}, null if not found
     */
    public static BomEntry getBomEntryWithUnitName(String unitName, List<BomEntry> list) {
        return list.stream()
                .filter(entry -> unitName.equals(entry.getUnit().getName()))
                .findAny()
                .orElse(null);
    }
    
    /**
     * Returns a String that contains all entries from {@code list} formatted as follows:
     * entries are separated with ";", each entry: unitName:amount
     * 
     * @param list list of BomEntry to format
     * @return "EMPTY" if {@code list} is empty, formatted String if entries available  
     */
    public static String formattedBomEntryList(List<BomEntry> list) {
        Objects.requireNonNull(list);
        
        //if no entry available return "EMPTY"
        if (list.isEmpty()) {
            return MessageStrings.EMPTY.toString();
        }
        
        //build result
        StringJoiner sj = new StringJoiner(";");
        for (BomEntry entry : list) {
            sj.add(entry.toString());
        }
        return sj.toString();
    }
}
