package edu.kit.informatik.analysis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import edu.kit.informatik.analysis.integrity.BomIntegrity;
import edu.kit.informatik.analysis.integrity.IntegrityChecker;
import edu.kit.informatik.analysis.ui.InputException;
import edu.kit.informatik.analysis.ui.MessageStrings;
import edu.kit.informatik.analysis.util.Util;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class BomManagement {
    /**
     * A Unit can contain max. this amount of a sub unit directly
     */
    public static final int MAX_DIRECT_SUB_AMOUNT = 1000;
    //contains all assemblies and components
    //the units contain references to their sub units (product structure)
    //so all changes that are made to the elements in inventory will be automatically
    //available in every unit that uses it (e.g. add/remove parts/assemblies)
    private List<Unit> inventory; 
    //implementation to perform integrity operations on the inventory
    private BomIntegrity integrityCheck;
    
    /**
     * Constructor to create a new BomManagement object
     */
    public BomManagement() {
        init();
    }
    
    /*
     * Inits the object
     */
    private void init() {
        this.inventory = new ArrayList<Unit>();
        this.integrityCheck = new IntegrityChecker();
    }
    
    /**
     * Adds an assembly to the inventory
     * Performs integrity checks on parameters and refuses to add if integrity would be violated
     * 
     * @param name name of assembly to add
     * @param subAssemblies subassemblies to add to the new assembly
     * @throws InputException if assembly with specified {@code name} already exists or 
     * a subassembly name occurs multiple times in {@code subassemblies} or
     * the new assembly would create a cycle in the product structure
     */
    public void addAssembly(String name, List<BomEntry> subAssemblies) throws InputException {
        Objects.requireNonNull(name);
        Objects.requireNonNull(subAssemblies);
        
        boolean addToInventory = false;
        
        //fetch unit from inventory if present
        Unit assembly = getUnitWithName(name);
        
        //if unit exists and is assembly -> error already exists
        if (assembly != null && assembly.getType() == UnitType.ASSEMBLY) {
            throw new InputException(MessageStrings.ASSEMBLY_ALREADY_EXISTS.toString());
        }
        
        //validate subAssemblies: check for duplicate names
        if (Util.duplicateUnitsInBomEntryList(subAssemblies)) {
            throw new InputException(MessageStrings.NAME_DUPLICATES.toString());
        }
        
        //if assembly with name does not exist in the system create a new one and add it to inventory
        //if cycle check doesn't fail
        if (assembly == null) {
            assembly = new Unit(name);
            addToInventory = true;
        }
                
        //Fetch existing units from the inventory
        List<BomEntry> existingUnitsToAdd = new ArrayList<>();
        List<BomEntry> newUnitsToAdd = new ArrayList<>();
        for (BomEntry entry : subAssemblies) {
            //get all units which should be added as sub units from the system, if already existent
            Unit sub = getUnitWithName(entry.getUnit().getName());           
            if (sub != null) {
                existingUnitsToAdd.add(new BomEntry(sub, entry.getUnitAmount())); //if present in system, use this
            } else {
                newUnitsToAdd.add(entry); //if not present in system, use a new one
            }
        }
        
        //check for cycles
        if (integrityCheck.checkForCycle(assembly, 
                Util.bomListToUnitList(Util.mergeLists(existingUnitsToAdd, newUnitsToAdd)))) {
            throw new InputException(MessageStrings.CYCLE_ERROR.toString());
        }
        
        //only add to inventory if not already present as component
        if (addToInventory) {
            this.inventory.add(assembly);
        } 
        
        //add existing subs to assembly
        for (BomEntry entry : existingUnitsToAdd) {
            assembly.addSubUnit(entry);
        }
        
        //add new subs to inventory and to assembly
        for (BomEntry entry : newUnitsToAdd) {
            this.inventory.add(entry.getUnit());
            assembly.addSubUnit(entry);
        } 
    }
    
    /**
     * Removes the assembly with specified {@code name}
     * If assembly is used in other assembly it will be a component
     * otherwise it will be removed from system
     * 
     * @param name name of assembly to remove
     * @throws InputException if assembly with given name does not exist
     */
    public void removeAssembly(String name) throws InputException {
        Objects.requireNonNull(name);
        
        Unit unit = getUnitWithName(name);
        
        if (unit == null || unit.getType() != UnitType.ASSEMBLY) {
            throw new InputException(MessageStrings.ASSEMBLY_NOT_EXIST.toString());
        }
        
        //save sub components for integrity check to remove all unused components
        //-> only the components that got just removed must be checked
        List<BomEntry> subComponents = unit.getDirectSubUnits(UnitType.COMPONENT);
        
        //turns assembly into component
        unit.removeAllSubUnits();
        
        //the currently removed unit must also be checked
        List<Unit> componentsToCheck = Util.bomListToUnitList(subComponents);
        componentsToCheck.add(unit);
        
        //if a component is not used by another assembly, it will be removed
        integrityCheck.removeUnusedComponents(componentsToCheck, this.inventory);
    }
    
    /**
     * Returns the direct units of the unit with the given {@code name}
     * 
     * @param name name of unit to print
     * @return String with all direct sub units, "COMPONENT" if unit with {@code name} is a component
     * @throws InputException if no unit with the given {@code name} exists in the system.
     */
    public String printAssembly(String name) throws InputException {
        Objects.requireNonNull(name);
        
        //try to get the unit with the given name
        Unit unit = getUnitWithName(name);
        
        if (unit == null) {
            throw new InputException(MessageStrings.UNIT_NOT_EXIST.toString());
        }
        
        return unit.toString();
    }
    
    /**
     * Returns all components with their amount used by the assembly with name {@code assemblyName} 
     * as formatted String
     * 
     * @param assemblyName name of assembly from which to get bill of quantity of components
     * @return bill of quantity of components as formatted String, sorted after amount descending, 
     * then unit name ascending
     * @throws InputException if assembly with name {@codeassemblyName} does not exist in system
     */
    public String getComponents(String assemblyName) throws InputException {
        Objects.requireNonNull(assemblyName);
        if (!assemblyExists(assemblyName)) {
            throw new InputException(MessageStrings.ASSEMBLY_NOT_EXIST.toString());
        }
        
        Unit unit = getUnitWithName(assemblyName);
        
        return Util.formattedBomEntryList(unit.getComponents());     
    }
    
    /**
     * Returns all assemblies with their amount used by the assembly with name {@code assemblyName} 
     * as formatted String
     * 
     * @param assemblyName name of assembly from which to get bill of quantity of assemblies
     * @return bill of quantity of assemblies as formatted String, sorted after amount descending, 
     * then unit name ascending
     * @throws InputException if assembly with name {@codeassemblyName} does not exist in system
     */
    public String getAssemblies(String assemblyName) throws InputException {
        Objects.requireNonNull(assemblyName);
        if (!assemblyExists(assemblyName)) {
            throw new InputException(MessageStrings.ASSEMBLY_NOT_EXIST.toString());
        }
        
        Unit unit = getUnitWithName(assemblyName);
        
        return Util.formattedBomEntryList(unit.getAssemblies());     
    }
    
    /**
     * Adds a given unit in the specified amount to an existing BOM
     * Performs integrity checks on parameters and refuses to add if integrity would be violated
     * 
     * @param assemblyNameToAddTo name of the BOM to which to add a part
     * @param toAdd unit and amount to add
     * @throws InputException if assembly with given name does not exist in system, 
     * if the addition of the part would exceed the maximum allowed number of a direct sub unit,
     * if the addition would create a cycle in the product structure
     */
    public void addPart(String assemblyNameToAddTo, BomEntry toAdd) throws InputException {
        Objects.requireNonNull(assemblyNameToAddTo);
        Objects.requireNonNull(toAdd);
        
        boolean addToInventory = false;
        
        //check if such an assembly exists
        if (!assemblyExists(assemblyNameToAddTo)) {
            throw new InputException(MessageStrings.ASSEMBLY_NOT_EXIST.toString());
        }
        
        Unit unitToAddTo = getUnitWithName(assemblyNameToAddTo);
        //gets unit to add from inventory if present
        //if not a new unit must be added to inventory after cycle check
        Unit unitToAdd = getUnitWithName(toAdd.getUnit().getName());
        if (unitToAdd == null) {
            unitToAdd = toAdd.getUnit();
            addToInventory = true;
        }
        
        //get all sub units of unit to which should be added
        List<BomEntry> subs = unitToAddTo.getDirectSubUnits(UnitType.EITHER);
        //get sub entry to add of unit to add to 
        //if existing, check amount, if not existing amount is already checked by regex input check
        BomEntry subToAddTo = Util.getBomEntryWithUnitName(unitToAdd.getName(), subs);
        if (subToAddTo != null) {
            if (subToAddTo.getUnitAmount() + toAdd.getUnitAmount() > MAX_DIRECT_SUB_AMOUNT) {
                throw new InputException(MessageStrings.TOO_MANY_SUBS.toString());
            }
        }    
        
        //check for cycles
        if (integrityCheck.checkForCycle(unitToAddTo, Arrays.asList(unitToAdd))) {
            throw new InputException(MessageStrings.CYCLE_ERROR.toString());
        }
        
        //add new unit to inventory if it wasn't already there
        if (addToInventory) {    
            this.inventory.add(unitToAdd);    
        } 
        
        //add unit as sub unit
        unitToAddTo.addSubUnit(new BomEntry(unitToAdd, toAdd.getUnitAmount()));
    }
    
    /**
     * Removes a given unit in the specified amount from an existing BOM.
     * If a component is completely removed and not used by another BOM, it will be removed from system
     * If assembly from which got removed is component afterwards, it will be checked too.
     * 
     * @param assemblyNameToRemoveFrom name of the assembly to remove the given amount of the given part
     * @param toRemove contains amount and part to remove
     * @throws InputException assembly with name {@code assemblyToRemoveFrom} does not exist in system,
     * if more parts should be removed than available,
     * if assembly with name {@code assemblyNameToRemoveFrom} doesn't contain the part to remove at all
     */
    public void removePart(String assemblyNameToRemoveFrom, BomEntry toRemove) throws InputException {
        Objects.requireNonNull(assemblyNameToRemoveFrom);
        Objects.requireNonNull(toRemove);
        
        //check if such an assembly exists
        if (!assemblyExists(assemblyNameToRemoveFrom)) {
            throw new InputException(MessageStrings.ASSEMBLY_NOT_EXIST.toString());
        }
        
        //get assembly from which to remove a part from inventory
        Unit assemblyToRemoveFrom = getUnitWithName(assemblyNameToRemoveFrom);
        //get the sub unit to remove
        List<BomEntry> subs = assemblyToRemoveFrom.getDirectSubUnits(UnitType.EITHER);
        BomEntry subToRemove = Util.getBomEntryWithUnitName(toRemove.getUnit().getName(), subs);
        if (subToRemove != null) {
            //check if amount is ok
            if (subToRemove.getUnitAmount() - toRemove.getUnitAmount() < 0) {
                throw new InputException(MessageStrings.TOO_FEW_SUBS.toString());
            }
            //remove part
            assemblyToRemoveFrom.removeSubUnit(toRemove);
         
            //remove unused components from system
            //unused components can only occur if the part was fully removed
            //then the removed part must be checked if it's a component
            //and if the unit from which the part got removed is now a component it must also be checked
            if (subToRemove.getUnitAmount() == toRemove.getUnitAmount()) {
                if (subToRemove.getUnit().getType() == UnitType.COMPONENT) {
                    if (assemblyToRemoveFrom.getType() == UnitType.COMPONENT) {
                        this.integrityCheck.removeUnusedComponents(
                                Arrays.asList(subToRemove.getUnit(), assemblyToRemoveFrom), this.inventory);
                    } else {
                        this.integrityCheck.removeUnusedComponents(
                                Arrays.asList(subToRemove.getUnit()), this.inventory);
                    }
                }                               
            }   
        } else {
            //unit does not contain the requested unit
            throw new InputException(MessageStrings.PART_NOT_AVAILABLE.toString());
        }
    }

    /*
     * Checks if a assembly with the given name exists in inventory,
     * return true if it exists, false if not
     */
    private boolean assemblyExists(String assemblyName) {
        Unit unit = getUnitWithName(assemblyName);
        return unit != null && unit.getType() == UnitType.ASSEMBLY;
    }

    /*
     * Gets the unit with the specified name from the inventory
     * returns null if not found
     */
    private Unit getUnitWithName(String name) {
        return this.inventory.stream()
            .filter(unit -> name.equals(unit.getName()))
            .findAny()
            .orElse(null);
    } 
}
