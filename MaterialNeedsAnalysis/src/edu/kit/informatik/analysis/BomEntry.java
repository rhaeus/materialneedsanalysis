/**
 * 
 */
package edu.kit.informatik.analysis;

import java.util.Objects;

/**
 * Represents an Entry in a BOM
 * Holds the used unit and the used amount of the unit
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class BomEntry implements Comparable<BomEntry> {
    private Unit unit;
    private long unitAmount; //used amount of the unit
    
    /**
     * Constructor to create a new BomEntry object
     * 
     * @param unit unit to store in this entry
     * @param amount amount of the unit. It is limited to positive values only.
     * If value <= 0 provided an IllegalArgumentException is thrown
     */
    public BomEntry(Unit unit, long amount) {
        Objects.requireNonNull(unit);
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount must be a positive value (>0)");
        }
        
        this.unit = unit;
        this.unitAmount = amount;
    }
    
    /**
     * Gets the unit of this BOM Entry
     * 
     * @return unit of this BOM Entry
     */
    public Unit getUnit() {
        return this.unit;
    }
    
    /**
     * Gets the amount of usage of the unit
     * 
     * @return required amount of the unit
     */
    public long getUnitAmount() {
        return this.unitAmount;
    }
    
    /**
     * Adds the given {@code amount}
     * If {@code amount} is negative, amount will be decreased, 
     * if positive amount will be increased
     * 
     * @param amount amount to add
     * @return true if possible, false if not (if more should be decreased than available)
     */
    public boolean addUnitAmount(long amount) {
        Objects.requireNonNull(amount);
        
        assert getUnitAmount() + amount >= 0;
        
        if (getUnitAmount() + amount < 0) {
            return false;
        }
        this.unitAmount += amount;
        return true;
    }
    
    @Override 
    public String toString() {
        return this.unit.getName() + ":" + this.unitAmount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (unitAmount ^ (unitAmount >>> 32));
        result = prime * result + ((unit == null) ? 0 : unit.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BomEntry other = (BomEntry) obj;
        if (unitAmount != other.unitAmount)
            return false;
        if (unit == null) {
            if (other.unit != null)
                return false;
        } else if (!unit.equals(other.unit))
            return false;
        return true;
    }

    @Override
    public int compareTo(BomEntry o) {
        //First criteria is amount: descending
        //Second criteria (if amount is equal) is name of unit: ascending
        if (this.unitAmount == o.unitAmount) {
            return this.getUnit().compareTo(o.getUnit());
        } else {
            return Long.compare(o.unitAmount, this.unitAmount);
        }
    }   
}
