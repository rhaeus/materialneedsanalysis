/**
 * 
 */
package edu.kit.informatik.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import edu.kit.informatik.analysis.util.Util;

/**
 * Represents a Unit (Assembly/Component)
 * Consists of a name and it's sub assemblies/components
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class Unit implements Comparable<Unit> {
    
    private String name; //name of this unit
    //all sub units used to assembly this unit. 
    //If empty, this unit is considered as component, otherwise as assembly
    //as key the name of the unit is used
    private Map<String, BomEntry> subAssembly; 
    
    /**
     * Constructor to create a new Unit object
     * 
     * @param name name of the Unit
     */
    public Unit(String name) {
        Objects.requireNonNull(name);
        this.name = name;
        this.subAssembly = new TreeMap<>(); //implement as tree map to get ordering of the elements
    }
    
    /**
     * Gets the name of the Unit
     * 
     * @return name of the unit
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Adds a sub unit (assembly or component) with the required amount to this unit
     * If the unit to add is already present, the amounts are added up
     * 
     * @param entry unit with it's amount to add
     * @return true if adding was successful, false if not 
     */
    public boolean addSubUnit(BomEntry entry) {
        Objects.requireNonNull(entry);
        
        //if entry does not exist already -> add
        //if it exists, add up amount
        if (!this.subAssembly.containsKey(entry.getUnit().getName())) {
            this.subAssembly.put(entry.getUnit().getName(), entry);
            return true;
        } else {
            BomEntry existingEntry = this.subAssembly.get(entry.getUnit().getName());
            boolean result = existingEntry.addUnitAmount(entry.getUnitAmount());
            
            assert result && existingEntry.getUnitAmount() <= BomManagement.MAX_DIRECT_SUB_AMOUNT;
            
            return result;
        }
    }
    
    /**
     * Removes a given amount of a sub unit from this unit if possible
     * 
     * @param entry the unit and amount to remove
     * @return true, if remove possible and successful, false if not 
     * (if this unit doesn't contain the requested unit or not enough amount is available)
     */
    public boolean removeSubUnit(BomEntry entry) {
        Objects.requireNonNull(entry);
        
        //if entry does not exist return false
        //if it exists, subtract amount if enough available, if not, return false
        //if amount is zero afterwards, remove sub unit
        if (this.subAssembly.containsKey(entry.getUnit().getName())) {
            BomEntry existingEntry = this.subAssembly.get(entry.getUnit().getName());
            if (existingEntry.getUnitAmount() == entry.getUnitAmount()) {
                //all amount will be removed -> remove entire entry from subassembly
                this.subAssembly.remove(entry.getUnit().getName());
            } else if (existingEntry.getUnitAmount() > entry.getUnitAmount()) {
                //enough amount available -> subtract amount to remove
                existingEntry.addUnitAmount(-1 * entry.getUnitAmount());
            } else {
                //not enough amount available -> remove not possible
                assert true; //should not happen in this program
                return false;
            }
        } else {
            //unit does not contain the requested sub unit
            assert true; //should not happen in this program
            return false;
        }
        return true;
    }
    
    /**
     * Removes all sub assemblies and sub components.
     * Unit is considered as component afterwards.
     */
    public void removeAllSubUnits() {
        assert !this.subAssembly.isEmpty();      
        this.subAssembly.clear();
    }
    
    /**
     * Gets the type of this Unit
     * 
     * @return {@code UnitType.COMPONENT} if this Unit has no subassemblies -> is component,
     * {@code UnitType.ASSEMBLY} if this Unit has subassmblies -> is assembly
     */
    public UnitType getType() {
        if (this.subAssembly.isEmpty()) {
            return UnitType.COMPONENT;
        } else {
            return UnitType.ASSEMBLY;
        }
    }
    
    /**
     * Checks if this Unit uses a component or assembly with name {@code name} directly
     * 
     * @param name name of the Unit to check if it is used
     * @return true if used, false if not
     */
    public boolean usesUnitWithNameDirectly(String name) {
        return this.subAssembly.containsKey(name);
    }
    
    /**
     * Gets a list of all components that are used by this Unit directly and indirectly
     * with merged entries: each unit occurs only once with added up amount
     * 
     * @return List which contains all used components with correspondent amount of usage (merged) 
     */
    public List<BomEntry> getComponents() {
        return Util.createMergedEntryList(getComponentsRaw());
    }
    
    /**
     * Gets a list of all components that are used by this Unit directly and indirectly,
     * the entries in the list are not merged, that means a unit can occur multiple times with different amount.
     * To get merged List, use {@link #getComponents()} instead. The methods are split up to avoid unnecessary
     * merging in recursive call.
     * This method is recursively called to fetch all subcomponents.
     * 
     * @return List which contains all used components with correspondent amount of usage (not merged)
     */
    public List<BomEntry> getComponentsRaw() {
        //get direct sub assemblies. They are used to recursively get all components
        List<BomEntry> directAssemblies = getDirectSubUnits(UnitType.ASSEMBLY);
        List<BomEntry> result = new ArrayList<>();
        //add direct sub components to resulting list
        result.addAll(getDirectSubUnits(UnitType.COMPONENT));
        
        //recursively get all components by calling this method on all sub assemblies of this unit and so on
        for (BomEntry entry : directAssemblies) {
            //add components with appropriate amount
            result.addAll(Util.multiplyBomEntries(entry.getUnit().getComponentsRaw(), entry.getUnitAmount()));
        }
        return result;
    }
      
    /**
     * Gets a list of all assemblies that are used by this Unit directly and indirectly
     * with merged entries: each unit occurs only once with added up amount.
     * This method is recursively called to fetch all subassemblies.
     * 
     * @return List which contains all used assemblies with correspondent amount of usage (merged)
     */
    public List<BomEntry> getAssemblies() {
        return Util.createMergedEntryList(getAssembliesRaw());
    }
    
    /**
     * Gets a list of all assemblies that are used by this Unit directly and indirectly,
     * the entries in the list are not merged, that means a unit can occur multiple times with different amount.
     * To get merged List, use {@link #getAssemblies()} instead. The methods are split up to avoid unnecessary
     * merging in recursive call.
     * This method is recursively called to fetch all subassemblies.
     * 
     * @return List which contains all used assemblies with correspondent amount of usage (not merged)
     */
    public List<BomEntry> getAssembliesRaw() {
        //get direct sub assemblies. They are used to recursively get all components
        List<BomEntry> directAssemblies = getDirectSubUnits(UnitType.ASSEMBLY);
        List<BomEntry> result = new ArrayList<>();
        //add direct sub assemblies to resulting list
        result.addAll(directAssemblies);
        
        //recursively get all assemblies by calling this method on all sub assemblies of this unit and so on
        for (BomEntry entry : directAssemblies) {
            //add assemblies with appropriate amount
            result.addAll(Util.multiplyBomEntries(entry.getUnit().getAssembliesRaw(), entry.getUnitAmount()));
        }
        return result;
    }
    
    /**
     * Gets a list of all Units of type {@code type} that are used by this Unit directly
     * 
     * @param type if type is COMPONENT, all direct components are returned,
     * if type is ASSEMBLY, all direct assemblies are returned,
     * if type is EITHER, all direct assemblies and components are returned
     * @return List which contains all directly used Units of type {@code type}
     */
    public List<BomEntry> getDirectSubUnits(UnitType type) {
        List<BomEntry> result = new ArrayList<BomEntry>();
        for (Map.Entry<String, BomEntry> entry : this.subAssembly.entrySet()) {
            if (type == UnitType.EITHER || entry.getValue().getUnit().getType() == type) {
                result.add(entry.getValue());
            }
        }
        return result;
    }
    
    @Override
    public String toString() {
        switch (getType()) {
            case COMPONENT:
                //if unit is component, return COMPONENT
                return UnitType.COMPONENT.toString();
            case ASSEMBLY:
                //if unit is assembly, return list with all direct sub units with amount
                return Util.formattedBomEntryList(getDirectSubUnits(UnitType.EITHER));
            default:
                assert true; //should not happen
                return "Invalid type";
            }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Unit other = (Unit) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public int compareTo(Unit o) {
        //sort after name: ascending
        return this.getName().compareTo(o.getName());
    }
}
