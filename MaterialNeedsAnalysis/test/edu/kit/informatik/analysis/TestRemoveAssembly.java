/**
 * 
 */
package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class TestRemoveAssembly {
    private BomManagement bomMan = new BomManagement();
    /**
     * @throws java.lang.Exception v
     */
    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
        bomMan = new BomManagement();
    }

    /**
     * @throws java.lang.Exception v
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * 
     */
    @Test
    public void test2() {
        Terminal.writeInputBuffer("addAssembly A=1:B;2:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly D=3:A;1:B;2:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:1;C:2"));
        
        Terminal.writeInputBuffer("removeAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("removeAssembly D");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("removeAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly A=1:B;2:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly A=1:B;2:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly B=2:C;3:D");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:1"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("C:4;D:3"));
        
        Terminal.writeInputBuffer("removeAssembly B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("C:2;B:1"));
    }

}
