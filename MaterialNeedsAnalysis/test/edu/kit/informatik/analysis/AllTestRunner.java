package edu.kit.informatik.analysis;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class AllTestRunner {
    /**
     * 
     * @param args v
     */
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(AllTestsSuite.class);

        for (Failure failure : result.getFailures()) {
           System.out.println(failure.toString());
        }
          
        System.out.println(result.wasSuccessful());
     }
}
