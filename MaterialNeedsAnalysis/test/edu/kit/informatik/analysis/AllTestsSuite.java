package edu.kit.informatik.analysis;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   TestAddAssembly.class,
   TestAddPart.class,
   TestBench.class,
   TestBigNumbers.class,
   TestDelete.class,
   TestGetAssemblies.class,
   TestGetComponents.class,
   TestPrintAssembly.class,
   TestRemoveAssembly.class,
   TestRemovePart.class,
   TestSampleInteraction1.class,
   TestSampleInteraction2.class,
})

public class AllTestsSuite {
    
}
