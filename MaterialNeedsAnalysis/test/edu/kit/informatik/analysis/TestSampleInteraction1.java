package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

public class TestSampleInteraction1 {
    private BomManagement bomMan = new BomManagement();

    /**
     * 
     * @throws Exception b
     */
    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * 
     * @throws Exception b
     */
    @After
    public void tearDown() throws Exception {
        Terminal.testMode = false;
    }

    /**
     * 
     */
    @Test
    public void test() {
        addAssembly();
        removeAssembly();
        printAssembly();
        getAssemblies();
        getComponents();
        addPart();
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        removePart();
//        Terminal.writeInputBuffer("printAssembly A");
//        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
    }
    
    private void removePart() {
        Terminal.writeInputBuffer("removePart X-4:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("removePart X-1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("printAssembly X");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("A:2;Y:1;Z:1"));
        
        Terminal.writeInputBuffer("removePart X-2:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("removePart X-1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    private void addPart() {
        Terminal.writeInputBuffer("addPart X+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addPart X+2:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addPart X+1:X");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart Y+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart E+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    private void getAssemblies() {
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("E:6;B:2;C:1"));
        
        Terminal.writeInputBuffer("getAssemblies I");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("E:2;J:1"));
        
        Terminal.writeInputBuffer("getAssemblies E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
    }
    
    private void getComponents() {
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:6;H:6;D:2;F:1"));
        
        Terminal.writeInputBuffer("getComponents I");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("H:5;F:2;G:2;K:1"));
        
        Terminal.writeInputBuffer("getComponents E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:1;H:1"));
    }
    
    private void printAssembly() {
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
//        assertTrue(Terminal.readOutputBuffer().equals("B:2"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:1;E:2"));
        
        Terminal.writeInputBuffer("printAssembly E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:1;H:1"));
        
        Terminal.writeInputBuffer("printAssembly J");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("F:2;H:3;K:1"));
        
        Terminal.writeInputBuffer("printAssembly K");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly Y");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
    }
    
    private void removeAssembly() {
        Terminal.writeInputBuffer("removeAssembly Y");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly Y");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));  
        
//        Terminal.writeInputBuffer("removeAssembly X");
//        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    private void addAssembly() {
        Terminal.writeInputBuffer("addAssembly A=2:B;1:C");
//        Terminal.writeInputBuffer("addAssembly A=2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly B=1:D;2:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly C=2:E;1:F");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly E=1:G;1:H");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly I=2:E;1:J");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly J=3:H;2:F;1:K");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly X=1:Y;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly Y=1:X;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Y=1:Y;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Y=1:Z;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Y=2:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }

}
