/**
 * 
 */
package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class TestRemovePart {
    private BomManagement bomMan = new BomManagement();
    /**
     * @throws java.lang.Exception v
     */
    @Before
    public void setUp() throws Exception {
        bomMan = new BomManagement();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * @throws java.lang.Exception v
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("removePart A-2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=10:B;10:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removePart A-1:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removePart B-2:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("removePart C-2:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("removePart A-2:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:8;C:10"));

        Terminal.writeInputBuffer("removePart A-10:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:8;C:10"));
        
        Terminal.writeInputBuffer("removePart A-8:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("C:10"));
        
        Terminal.writeInputBuffer("removePart A-10:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10;Z:1000"));
        
        Terminal.writeInputBuffer("addAssembly A=10:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly C=2:A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("A:2"));
        
        Terminal.writeInputBuffer("removePart A-10:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("A:2"));
        
        Terminal.writeInputBuffer("removePart C-2:A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
    }

    /**
     * 
     */
    @Test
    public void test2() {
        Terminal.writeInputBuffer("addAssembly A=10:B;20:D");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10;D:20"));
        
        Terminal.writeInputBuffer("removePart A-20:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10;D:20"));
        
        Terminal.writeInputBuffer("removePart A-20:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10;D:20"));
        
        Terminal.writeInputBuffer("removePart A-20:D");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
    }
}
