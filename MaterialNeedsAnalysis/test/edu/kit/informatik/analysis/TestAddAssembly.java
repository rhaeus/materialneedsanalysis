/**
 * 
 */
package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class TestAddAssembly {
    private BomManagement bomMan = new BomManagement();
    /**
     * @throws java.lang.Exception b
     */
    @Before
    public void setUp() throws Exception {
        bomMan = new BomManagement();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * @throws java.lang.Exception b
     */
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * 
     */
    @Test
    public void testVarious() {
        Terminal.writeInputBuffer("addPart A+2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=1:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        //cant add part to component
        Terminal.writeInputBuffer("addPart B+1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart A+1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly C=2:D;3:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly C=2:D;3:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:1;C:1"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("C:1"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("E:3;D:2;B:1"));
    }
    
    /**
     * 
     */
    @Test
    public void testCycle() {
        Terminal.writeInputBuffer("addAssembly A=2:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=1:C;2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly B=1:D;2:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly E=1:H;1:G");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly H=1:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly H=1:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly H=1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly H=1:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
    }
    
    /**
     * 
     */
    @Test
    public void testCycle3() {
        Terminal.writeInputBuffer("addAssembly Y=1:X;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly X=1:Y;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }
    
    /**
     * 
     */
    @Test
    public void testCycle2() {
        Terminal.writeInputBuffer("addAssembly X=1:Y;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly Y=1:X;1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.printToConsoleInTestMode = false;
        Terminal.writeInputBuffer("addAssembly A=2:B;1:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=1000:B;1:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getAssemblies B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
               
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=1:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly B=1:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:2;C:1"));
        
    }
    
    /**
     * 
     */
    @Test
    public void testSyntax() {
        Terminal.printToConsoleInTestMode = false;
        Terminal.writeInputBuffer("addAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A.=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Akj6die389348r=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Akj6die389348r #kjg=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A:2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=-1:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=0:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=1001:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly A=2:B;1:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:2;C:1"));
        
        Terminal.writeInputBuffer("addAssembly B=1000:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("C:1000"));
    }   
}
