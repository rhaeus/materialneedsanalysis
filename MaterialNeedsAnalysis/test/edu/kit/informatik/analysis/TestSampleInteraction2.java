package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

public class TestSampleInteraction2 {
    private BomManagement bomMan = new BomManagement();
    
    /**
     * 
     * @throws Exception b
     */
    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * 
     * @throws Exception b
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("addAssembly A=10:B;12:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly a");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:10;C:12"));
        
        Terminal.writeInputBuffer("addAssembly B=18:E;15:D;27:F");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:15;E:18;F:27"));
        
        Terminal.writeInputBuffer("addAssembly C=11:E;18:D;52:G");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:18;E:11;G:52"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("C:12;B:10"));
        
        Terminal.writeInputBuffer("getAssemblies B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getAssemblied D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:624;D:366;E:312;F:270"));
        
        Terminal.writeInputBuffer("getComponents B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("F:27;E:18;D:15"));
        
        Terminal.writeInputBuffer("getComponents C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:52;D:18;E:11"));
        
        Terminal.writeInputBuffer("getComponents D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly D=9:F;12:G");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("F:9;G:12"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:366;C:12;B:10"));
        
        Terminal.writeInputBuffer("getAssemblies B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:15"));
        
        Terminal.writeInputBuffer("getAssemblies C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("D:18"));
        
        Terminal.writeInputBuffer("getAssemblies D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:5016;F:3564;E:312"));
        
        Terminal.writeInputBuffer("getComponents B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:180;F:162;E:18"));
        
        Terminal.writeInputBuffer("getComponents C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:268;F:162;E:11"));
        
        Terminal.writeInputBuffer("getComponents D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("G:12;F:9"));
        
        Terminal.writeInputBuffer("addPart A+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart D+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart E+1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly E=1:A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("removeAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("removeAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("B:10;C:12"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("C:12;B:10"));
        
        Terminal.writeInputBuffer("printAssembly B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("printAssembly D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("F:9;G:12"));
        
        Terminal.writeInputBuffer("printAssembly E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Abc=1906:iJk");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly Abc=19:iJk");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("iJk:19"));
        
        Terminal.writeInputBuffer("addAssembly iJk=6:xyZ");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly iJk");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("xyZ:6"));
        
        Terminal.writeInputBuffer("getAssemblies Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("iJk:19"));
        
        Terminal.writeInputBuffer("getComponents Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("xyZ:114")); 
        
        Terminal.writeInputBuffer("addPart Abc+1:xyZ");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK")); 
        
        Terminal.writeInputBuffer("printAssembly Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("iJk:19;xyZ:1"));   
        
        Terminal.writeInputBuffer("addPart iJk+1:xyZ");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK")); 
        
        Terminal.writeInputBuffer("removePart iJk-7:xyZ");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));        
        Terminal.writeInputBuffer("getAssemblies Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY")); 
        
        Terminal.writeInputBuffer("getComponents Abc");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("iJk:19;xyZ:1"));
    }

}
